package com.ctis.testautomation.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ctis.testautomation.dto.PersonInfo;

@Controller
@RequestMapping(path = "/person")
public class PersonController {

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ModelAndView getPersonInfo(@RequestBody PersonInfo personInfo) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("perName", personInfo.getFullName());
		mv.setViewName("person");
		return mv;
	}

}
