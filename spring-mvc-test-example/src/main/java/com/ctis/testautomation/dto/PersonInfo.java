package com.ctis.testautomation.dto;

import org.apache.commons.lang3.StringUtils;

public class PersonInfo {
	private String title;

	private String firstName;

	private String lastName;

	// Needed for Jackson api
	private PersonInfo() {

	}

	public PersonInfo(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public PersonInfo(String title, String firstName, String lastName) {
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isNotBlank(this.title)) {
			builder.append(this.title).append(". ");
		}
		builder.append(this.firstName).append(" ").append(this.lastName);
		return builder.toString();
	}
}
