package com.ctis.testautomation.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringJUnitWebConfig(locations = { "classpath:mvc-config-test.xml" })
public class PersonControllerIntTest {

	@InjectMocks
	PersonController controller;

	@Autowired
	private WebApplicationContext webAppContext;

	private MockMvc mockMvc;

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
	}

	@Test
	public void testGetPersonInfo_givenValidPersonInfo_returnsMV() throws Exception {
		String sampleJson = "{\"title\":\"Ms\",\"firstName\":\"Jane\",\"lastName\":\"Doe\"}";
		mockMvc.perform(post("/person").contentType(MediaType.APPLICATION_JSON_UTF8).content(sampleJson))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(model().attribute("perName", "Ms. Jane Doe")).andExpect(view().name("person"));
	}
}
