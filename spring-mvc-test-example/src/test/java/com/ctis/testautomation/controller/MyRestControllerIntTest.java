package com.ctis.testautomation.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:mvc-config-test.xml" })
@WebAppConfiguration
public class MyRestControllerIntTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	@Mock
	MyRestController controller;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetText() throws Exception {
		mockMvc.perform(get("/rest/get/text") //
		).andDo(MockMvcResultHandlers.print()) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN)) //
				.andExpect(content().string(equalTo(("Spring MVC - REST Controller Hello World example."))));
	}

	@Test
	public void testGetJson() throws Exception {
		mockMvc.perform(get("/rest/get/json") //
		).andDo(MockMvcResultHandlers.print()) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE)) //
				.andExpect(jsonPath("$[0]").value("One")); //
	}

	@Test
	public void testGetXml() throws Exception {
		String xmlContent = "<user><id>12</id><name>John</name></user>";
		mockMvc.perform(get("/rest/get/xml") //
		).andDo(MockMvcResultHandlers.print()) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML_VALUE)) //
				.andExpect(xpath("user/name").string(equalTo("John"))).andExpect(content().xml(xmlContent));
	}
}
