package com.ctis.testautomation.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.web.servlet.ModelAndView;

import com.ctis.testautomation.dto.PersonInfo;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {

	@InjectMocks
	PersonController controller;

	@Test
	public void testGetPersonInfo_givenValidPersonInfo_returnsMV() throws Exception {
		PersonInfo personInfo = new PersonInfo("Ms", "Jane", "Doe");
		ModelAndView mv = controller.getPersonInfo(personInfo);
		ModelAndViewAssert.assertViewName(mv, "person");
		ModelAndViewAssert.assertModelAttributeValue(mv, "perName", "Ms. Jane Doe");
	}
}
